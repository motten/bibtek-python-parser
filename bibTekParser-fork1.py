#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 20:38:02 2013

@author: otter
"""
import sys, getopt

class reference(object):
    changeFlag    = False
    beginningLine = 0
    endLine       = 0
    authors       = ''
    journal       = ''
    year          = ''



def main(argv):

    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print 'bibTekParser.py -i <inputfile> -o <outputfile>' 
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'bibTekParser.py -i <inputfile> -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg


    references  = []
    k = 0
    references.append(reference())
    fileInput   = open(inputfile, 'r')
    fileOutput  = open(outputfile, 'w')
    searchlines = fileInput.readlines()
    #Read in the file as a list of references
    for i, line in enumerate(searchlines):
        if "@" == line[0] and "{" in line:
            references.append(reference())
            references[k].beginningLine = i
            #print line
            #print k
        if "}" == line[0] and "\n" == line[1]:
            references[k].endLine = i
            #print k
            k+=1

    #Assign values to the references[k] object from the bibTek file
    for k in range(len(references)):
        #Only look inside the reference
        for i in range(references[k].beginningLine,references[k].endLine):
            if "isi:" in searchlines[i].lower():
                references[k].changeFlag = True
            if "author = " in searchlines[i].lower():
                if "," != searchlines[i][-2]:
                    authorString = searchlines[i][:-1] + searchlines[i+1][2:]
                else:
                    authorString = searchlines[i]
                index = authorString.index("=")
                while (authorString[index] == "\"" or authorString[index] == "{"
                or authorString[index] == " " or authorString[index] == "="):
                    index += 1
                #Check for 2 letter last names
                if "," in authorString[index:index+3]:
                    references[k].authors = authorString[index:index+2]
                else:
                    references[k].authors = authorString[index:index+3]    
                index = 0
                a = 1
                while(a == 1):
                    try:
                        index = authorString.index("and ",index+1)
                        #check for two character last names
                        if "," in authorString[index+4:index+7]:
                            references[k].authors += authorString[index+4:index+6]
                        else:
                            references[k].authors += authorString[index+4:index+7]
                    except:
                        a = 0

                    
            if "journal = " in searchlines[i].lower():
                if "," != searchlines[i][-2]:
                    journalString = searchlines[i][:-1] + searchlines[i+1][2:]
                else:
                    journalString = searchlines[i]
                index = journalString.index("=")
                while (journalString[index] == "\"" or journalString[index] == "{"
                or journalString[index] == " " or journalString[index] == "="):
                    index += 1
                references[k].journal = journalString[index]
                a = 1
                while(a == 1):
                    try:
                        index = journalString.index(" ",index+1)
                        #print index
                        #print journalLine[i][index+1:index+3]
                        if(journalString[index+1] != '\\' and journalString[index+1:index+3] != ('OF') and 
                        journalString[index+1:index+3] != 'IN' and journalString[index+1:index+4] != 'AND'
                        and journalString[index+1:index+4] != 'THE' and journalString[index+1:index+3] != 'ON'):
                            references[k].journal += journalString[index+1]
                    except:
                        a = 0
                                
                
            if "year = " in searchlines[i].lower():
                index = searchlines[i].index('=')
                while (searchlines[i][index] == "\"" or searchlines[i][index] == "{"
                or searchlines[i][index] == " "  or searchlines[i][index] == "="):
                    index += 1
                references[k].year = (searchlines[i][index:index+2])
                
    #Change the lines that were read in based on the references[i].changeFlag
    for i in range(len(references)):
        if references[i].changeFlag == True:
            line = searchlines[references[i].beginningLine]
            if "@inproceedings{" in line.lower():
                k =1
            if "@unpublished{" in line.lower():
                k =1
            if "@misc{" in line.lower():
                k =1
            if "@book{" in line.lower():
                k =1
            if "@incollection{" in line.lower():
                k =1
            if "@article{" in line.lower():
                line = "@article{ " + references[i].authors + '-' + references[i].journal + '-' + references[i].year + ',\n'
                searchlines[references[i].beginningLine] = line
            
    #print to output file
    for i, line in enumerate(searchlines):
        fileOutput.write(line)
    

if __name__ == "__main__":
   main(sys.argv[1:])
